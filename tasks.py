__author__ = 'dredregg'
from celery import Celery
from log_buffer.batches import Batches
from log_buffer.log_web_controller import Log, pull_logs

#celery_app = Celery('log_buff', broker='redis://localhost:6379/0', worker_prefetch_multiplier  = 400)

celery_app = Celery('log_buff')
celery_app.config_from_object('celeryconfig')

@celery_app.task(base=Batches, flush_every=15, flush_interval=100)
def buffer_log(log_requests):
    logs = [Log.from_request(r) for r in log_requests]
    pull_logs(logs=logs)

