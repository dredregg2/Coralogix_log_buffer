
===============================================
coralogix log buffer - Buffer for coralogix logs
===============================================


Setup
===================

1.Create venv

2.Run venv

3.Install requirements::

    pip install -r requirements.txt


4.Install redis

5.Run redis(port 6379)::

    redis-server



6.Run workers::

     cd сoralogix_log_buffer
     celery worker -A tasks --loglevel=debug --concurrency=1



7.Run main.py to add new logs to buffer
