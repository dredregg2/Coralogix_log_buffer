__author__ = 'dredregg'
import requests
import json

PRIVATE_KEY = "444b11aa-b040-bf43-2659-41d556844f5f"
CORALOGIX_SENT_URL = 'https://api.coralogix.com/api/v1/logs/'

LOG_TYPE_DEBUG = 1
LOG_TYPE_VERBOSE = 2
LOG_TYPE_INFO = 3
LOG_TYPE_WARN = 4
LOG_TYPE_ERROR = 5
LOG_TYPE_CRITICAL = 6

APPLICATION_NAME = "APP1"
SUBSYSTEM_NAME = "DEFAULT_SUBSYSTEM"
COMPUTER_NAME = "COMPUTER_NAME"


class Log(object):
    #: log type  -- from constants
    log_type = None

    #log text

    text = None

    #log timestamp

    timestamp = None

    def __init__(self, text, log_type, timestamp):
        self.text = text
        self.log_type = log_type
        self.timestamp = timestamp

    @classmethod
    def from_request(cls, request):
        return cls(request.kwargs['text'], request.kwargs['log_type'], request.timestamp)

def pull_logs(logs, computer_name=COMPUTER_NAME, subsystem_name=SUBSYSTEM_NAME, application_name=APPLICATION_NAME):
    log_entries = []
    for log in logs:
        log_entries.append({"timestamp": log.timestamp,
                    "severity": log.log_type,
                    "text": log.text})


    r = requests.post(CORALOGIX_SENT_URL,
                      headers={'Content-Type':'application/json'},
                      data = json.dumps(
                      {
                       "privateKey": PRIVATE_KEY,
                       "applicationName": application_name,
                       "subsystemName": subsystem_name,
                       "computerName": computer_name,
                       "logEntries": log_entries
                       })
    )
    print("Pull logs request", r.text)

